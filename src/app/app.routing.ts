﻿import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { DeleteUserComponent } from './components/delete-user/delete-user.component';
import { EditComponent } from './components/edit/edit.component'
import { RegisterComponent } from './components/register/register.component'
import { AuthGuard } from './_guards';

const appRoutes: Routes = [
    { path: '', component: UsersComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);