import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { AuthenticationService } from '../_services';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    debugger
    if(req.method === 'PUT' || req.method === 'DELETE'){
     const authToken = this.auth.token;
        const authReq = req.clone({ setHeaders: { Authorization: authToken } });
        return next.handle(authReq);

    }else
    return next.handle(req);

  }
}


