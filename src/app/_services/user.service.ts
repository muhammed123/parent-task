﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserViewModel } from '../_models/user';


@Injectable()
export class UserService {
    selectedUser :UserViewModel;
    constructor(private http: HttpClient) { }

    apiUrl = "https://reqres.in/api/";
    getAll(page) {
        return this.http.get<UserViewModel[]>(this.apiUrl + "users?page="+page);
    }

    getById(id: number) {
        return this.http.get(this.apiUrl +`users/` + id);
    }

    register(user: UserViewModel) {
        return this.http.post(this.apiUrl + `users/register`, user);
    }

    update(id:number,user: UserViewModel) {
        return this.http.put(this.apiUrl + `users/` + id, user);
    }

    add(user: UserViewModel){
        return this.http.post(this.apiUrl + `users/`, user);
    }

    delete(id: number) {
        return this.http.delete(this.apiUrl + `users/` + id);
    }
}