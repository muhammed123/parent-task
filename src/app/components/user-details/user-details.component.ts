import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { EditComponent } from '../edit/edit.component';
import { UserService } from 'src/app/_services';
import { UserViewModel } from 'src/app/_models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @ViewChild('deleteUserPopUp') deleteUserPopUp: DeleteUserComponent;
  @ViewChild('editPopUp') editPopUp: EditComponent;
  @Output() hideDetailsSec: EventEmitter<any> = new EventEmitter();
  selectedUser:UserViewModel;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.selectedUser=this.userService.selectedUser;
  }

  openDelete() {
    this.deleteUserPopUp.show(this.selectedUser);
  }
  openEdit() {
    this.editPopUp.show(this.selectedUser);
  }
  showDetails(){
    this.selectedUser = this.userService.selectedUser;
    this.selectedUser.viewDetails=true;
  }
  hideDetails(){
    this.selectedUser.viewDetails=false;
    this.selectedUser = null;
    this.hideDetailsSec.emit();
  }
}
