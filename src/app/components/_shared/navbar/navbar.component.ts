import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  currentUser:any;
  showLogin:boolean = true;
  constructor(private authenticationService:AuthenticationService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser){
      this.showLogin=false;
    }
  }
logout(){
  this.authenticationService.logout();
  window.location.reload(true)
  this.showLogin = true;
}
}
