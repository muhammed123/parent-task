import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalBasicComponent } from '../_shared/modal-basic/modal-basic.component';
import { UserViewModel } from 'src/app/_models/user';
import { UserService, AlertService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {
  @ViewChild("basicModel") basicModel: ModalBasicComponent;
  @Output() onDelete: EventEmitter<any> = new EventEmitter();


  userToDelete: UserViewModel;
  constructor(private userService: UserService,private alertService:AlertService) { }
  ngOnInit() {
    this.userToDelete = new UserViewModel();
  }
  show(user) {
    this.basicModel.hideHeader = true;
    this.userToDelete = user;
    this.basicModel.show();
  }

  delete() {
    let id = this.userToDelete.id
    this.userService.delete(id).pipe(first()).subscribe(() => {
      this.onDelete.emit();
      this.basicModel.hide();
      this.alertService.success("deleted Successfully"); 
       },error => {
        this.alertService.error(error);
    });
  }

}
