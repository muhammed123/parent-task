import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalBasicComponent } from '../_shared/modal-basic/modal-basic.component';
import { UserViewModel } from 'src/app/_models/user';
import { UserService, AlertService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @ViewChild("basicModel") basicModel: ModalBasicComponent;
  @Output() onAdd: EventEmitter<any> = new EventEmitter();
  addForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  constructor(private userService: UserService,
    private alertService:AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
    ) { 

  }

  ngOnInit() {
    this.basicModel.hideHeader = true;
    this.addForm = this.formBuilder.group({
        name: ['', Validators.required],
        job: ['', Validators.required]
    });
  }

  show() {
    this.basicModel.show();
  }

  get f() { return this.addForm.controls; }

  edit() {
    this.loading = true;
      this.userService.add(this.addForm.value).pipe(first()).subscribe(() => {
      this.onAdd.emit();
      this.loading=false;
      this.basicModel.hide();
      this.alertService.success("Added Successfully"); 
       },error => {
        this.alertService.error(error);
        this.basicModel.hide();
        this.loading=false;
    });
  }
}
