import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalBasicComponent } from '../_shared/modal-basic/modal-basic.component';
import { UserViewModel } from 'src/app/_models/user';
import { UserService, AlertService } from 'src/app/_services';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @ViewChild("basicModel") basicModel: ModalBasicComponent;
  @Output() onEdit: EventEmitter<any> = new EventEmitter();
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  userToUpdate: UserViewModel;
  constructor(private userService: UserService,
    private alertService:AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
    ) { 

  }

  ngOnInit() {
    this.basicModel.hideHeader = true;
    this.userToUpdate = new UserViewModel();
    this.editForm = this.formBuilder.group({
        name: ['', Validators.required],
        job: ['', Validators.required]
    });
  }

  show(user) {
    this.userToUpdate = user;
    this.editForm.patchValue({
      name: this.userToUpdate.first_name
    })
    this.basicModel.show();
  }

  get f() { return this.editForm.controls; }

  edit() {
    this.loading = true;
    let id = this.userToUpdate.id
      this.userService.update(id,this.editForm.value).pipe(first()).subscribe(() => {
      this.onEdit.emit();
      this.loading=false;
      this.basicModel.hide();
      this.alertService.success("Updated Successfully"); 
       },error => {
        this.alertService.error(error);
        this.basicModel.hide();
        this.loading=false;
    });
  }

}
