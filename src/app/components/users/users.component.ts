import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { first } from 'rxjs/operators';

import { UserBindingModel, UserViewModel } from '../../_models/user'
import { UserService, AlertService } from '../../_services';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { EditComponent } from '../edit/edit.component';
import { AddComponent } from '../add/add.component';
import { UserDetailsComponent } from '../user-details/user-details.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  currentUser: any;
  users: UserViewModel[] = [];
  selectedUser: UserViewModel;
  openDetailsSec: boolean = false;
  listLoading: boolean = false;
  page: number = 0;

  scrolledUsers: UserViewModel[] = [];
  @ViewChild('deleteUserPopUp') deleteUserPopUp: DeleteUserComponent;
  @ViewChild('editPopUp') editPopUp: EditComponent;
  @ViewChild('addPopUp') addPopUp: AddComponent;
  @ViewChild('detailsSec') detailsSec: UserDetailsComponent;
  constructor(private userService: UserService, private alertService: AlertService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.selectedUser = new UserViewModel();
    let i = this.page
    for(i=0;i<2;i++){
      this.loadAllUsers();

    }

  }

  deleteUser(id: number) {
    debugger
    this.userService.delete(id).pipe(first()).subscribe((res) => {
      this.loadAllUsers()
    },
      error => {
        this.alertService.error(error);
      });
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.page++
      this.loadAllUsers();
    }
  }

  private loadAllUsers() {
    this.listLoading = true;
    this.userService.getAll(this.page).subscribe(users => {
      this.users = users['data'] as UserViewModel[];
      this.scrolledUsers.push(...this.users);
      this.listLoading = false
    });
  }

  openDelete(user) {
    this.deleteUserPopUp.show(user);
  }
  openEdit(user) {
    this.editPopUp.show(user);
  }
  openAdd() {
    this.addPopUp.show();
  }
  showDetails(user) {
    this.scrolledUsers.forEach(x => {
      x.viewDetails = false;
    })
    this.openDetailsSec = true;
    this.selectedUser = user;
    this.userService.selectedUser = user;
    this.detailsSec.showDetails()

  }

  closeDetails() {
    this.openDetailsSec = false;
    this.scrolledUsers.forEach(x => {
      x.viewDetails = false;
    })
    this.selectedUser = null;
  }


}
